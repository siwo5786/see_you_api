package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.exception.CAccessDeniedException;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.member.ProfileResponse;
import com.seeyou.seeyouapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final MemberRepository memberRepository;

    public Member getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Member member = memberRepository.findByUsername(username).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기
        if (!member.getIsEnabled()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다 던지기
        return member;
    }

    public ProfileResponse getProfile() {
        Member member = getMemberData();
        return new ProfileResponse.ProfileResponseBuilder(member).build();
    }
}
