package com.seeyou.seeyouapi.service.sellproduct;

import com.seeyou.seeyouapi.entity.SellProduct;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.sellproduct.SellProductItem;
import com.seeyou.seeyouapi.model.sellproduct.SellProductRequest;
import com.seeyou.seeyouapi.repository.SellProductRepository;
import com.seeyou.seeyouapi.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SellproductService {
    private final SellProductRepository sellProductRepository;

    public void setSellProduct(SellProductRequest sellProductRequest) {
        SellProduct sellProduct = new SellProduct.Builder(sellProductRequest).build();
        sellProductRepository.save(sellProduct);
    }
    public void putSellProduct(long id, SellProductRequest sellProductRequest) {
        SellProduct sellProduct = sellProductRepository.findById(id).orElseThrow(CMissingDataException::new);
        sellProduct.putSellProduct(sellProductRequest);
        sellProductRepository.save(sellProduct);
    }
    public ListResult<SellProductItem> getSellProductList(int pageNum) {
        Page<SellProduct> sellProducts = sellProductRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(pageNum, 20));

        List<SellProductItem> result = new LinkedList<>();

        for (SellProduct sellProduct : sellProducts.getContent()) {
            result.add(new SellProductItem.Builder(sellProduct).build());

        }

        return ListConvertService.settingResult(result, sellProducts.getTotalElements(), sellProducts.getTotalPages(), sellProducts.getPageable().getPageNumber());
    }
}
