package com.seeyou.seeyouapi.service.product;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.exception.CAccessDeniedException;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.product.ProductItem;
import com.seeyou.seeyouapi.model.product.ProductRequest;
import com.seeyou.seeyouapi.model.product.ProductUpdateRequest;
import com.seeyou.seeyouapi.repository.ProductRepository;
import com.seeyou.seeyouapi.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public Product getProductData(long id) {
        return productRepository.findById(id).orElseThrow(CAccessDeniedException::new);
    }

    public void setProduct(ProductRequest productRequest) {
        Product product = new Product.ProductBuilder(productRequest).build();
        productRepository.save(product);
    }

    public ListResult<ProductItem> getProductList(int pageNum) {
        // Page = List + 총 몇페이지인지 + 현재 몇페이지인지 + 총 몇개의 데이터를 가지고 있는지
        Page<Product> products = productRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(pageNum, 20));

        List<ProductItem> result = new LinkedList<>();
        // 하나씩 던져주면서 result에 더한다.
        for (Product product : products.getContent()) {
            // result는 ProductItem의 List
            result.add(new ProductItem.Builder(product).build());
        }
        //                                             어아탬 리스트 ,총 몇개의 데이터를 갖고 있는지, 총 몇페이지 인지, 현재페이지가 몇번인지
        return ListConvertService.settingResult(result, products.getTotalElements(), products.getTotalPages(), products.getPageable().getPageNumber());
    }

    public void putProduct(long id, ProductUpdateRequest productUpdateRequest) {
        Product product = productRepository.findById(id).orElseThrow(CMissingDataException::new);
        product.putProduct(productUpdateRequest);
        productRepository.save(product);
    }
}


