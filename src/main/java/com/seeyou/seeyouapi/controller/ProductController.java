package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.product.ProductItem;
import com.seeyou.seeyouapi.model.product.ProductRequest;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.product.ProductUpdateRequest;
import com.seeyou.seeyouapi.service.ResponseService;
import com.seeyou.seeyouapi.service.product.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(tags = "상품 관리")
@RequiredArgsConstructor
@RequestMapping(value = "/v1/product")
public class ProductController {
    private final ProductService productService;

    @PostMapping("/new")
    @ApiOperation(value = "상품 등록")
    public CommonResult setProduct(@RequestBody @Valid ProductRequest productRequest) {
        productService.setProduct(productRequest);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all")
    @ApiOperation(value = "리스트 불러오기")
    public ListResult<ProductItem> getProductList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(productService.getProductList(page), true);
    }
    @PutMapping("/update/{id}")
    @ApiOperation(value = "상품 수정")
    public CommonResult putProduct(@PathVariable long id, @RequestBody @Valid ProductUpdateRequest productUpdateRequest) {
        productService.putProduct(id, productUpdateRequest);
        return ResponseService.getSuccessResult();

    }
}