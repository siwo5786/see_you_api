package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.model.attendance.AttendanceItem;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.service.ProfileService;
import com.seeyou.seeyouapi.service.ResponseService;
import com.seeyou.seeyouapi.service.attendance.AttendanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
@RestController
@Api(tags = "근태 관리")
public class AttendanceController {
    private final AttendanceService attendanceService;
    private final ProfileService profileService;

    @PostMapping("/new")
    @ApiOperation("근태 등록")
    public CommonResult setAttendance() {
        Member member = profileService.getMemberData();
        attendanceService.setAttendance(member);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/leave-work/update")
    @ApiOperation("퇴근 하기")
    public CommonResult putLeaveWork() {
        Member member = profileService.getMemberData();
        attendanceService.putLeaveWork(member);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/list")
    @ApiOperation("리스트 불러오기")
    public ListResult<AttendanceItem> getAttendanceResult(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(attendanceService.getAttendanceResult(page), true);
    }
}
