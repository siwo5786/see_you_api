package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.SellProductType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.model.sellproduct.SellProductRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private SellProductType sellProductType;

    @Column(nullable = false, length = 20)
    private String productName;

    @Column(nullable = false)
    private Double unitPrice;

    @Column(nullable = false)
    private Boolean isEnabled;

    public void putSellProduct(SellProductRequest sellProductRequest) {
        this.sellProductType = sellProductRequest.getSellProductType();
        this.productName = sellProductRequest.getProductName();
        this.isEnabled = sellProductRequest.getIsEnabled();
        this.unitPrice = sellProductRequest.getUnitPrice();

    }

    private SellProduct(Builder builder) {
        this.sellProductType = builder.sellProductType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;

    }

    public static class Builder implements CommonModelBuilder<SellProduct> {

        private final SellProductType sellProductType;
        private final String productName;
        private final Double unitPrice;
        private final Boolean isEnabled;

        public Builder(SellProductRequest sellProductRequest) {
            this.sellProductType = sellProductRequest.getSellProductType();
            this.productName = sellProductRequest.getProductName();
            this.unitPrice = sellProductRequest.getUnitPrice();
            this.isEnabled = true;
        }


        @Override
        public SellProduct build() {
            return new SellProduct(this);
        }
    }

}



