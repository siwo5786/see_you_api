package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDate dateAttendance;

    @Column(nullable = false)
    private LocalTime timeAttendance;

    private LocalTime timeLeaveWork;

    // 퇴근까지 완료하면 true
    @Column(nullable = false)
    private Boolean isComplete;

    public void leaveWork() {
        this.timeLeaveWork = LocalTime.now();
    }

    private Attendance(Builder builder) {
        this.member = builder.member;
        this.dateAttendance = builder.dateAttendance;
        this.timeAttendance = builder.timeAttendance;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<Attendance> {

        private final Member member;
        private final LocalDate dateAttendance;
        private final LocalTime timeAttendance;
        private final Boolean isComplete;

        public Builder(Member member) {
            this.member = member;
            this.dateAttendance = LocalDate.now();
            this.timeAttendance = LocalTime.now();
            this.isComplete = false;

        }

        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }
}
