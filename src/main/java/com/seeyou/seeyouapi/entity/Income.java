package com.seeyou.seeyouapi.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Income {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer incomeYear;

    @Column(nullable = false)
    private Integer incomeMonth;

    @Column(nullable = false, length = 20)
    private String incomeCategory;

    @Column(nullable = false)
    private Double price;


}

