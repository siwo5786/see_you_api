package com.seeyou.seeyouapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
