package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    //    By는 where문 이기 떄문에 조건이 무조껀 들어가야함. 그래서 서비스에서 보면 id값이 1이상인 걸 모두 가져오라고 되어있다.
    Page<Product> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
