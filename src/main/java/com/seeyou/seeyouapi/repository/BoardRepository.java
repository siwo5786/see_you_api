package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Board;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {
}
