package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.entity.Sell;
import com.seeyou.seeyouapi.entity.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockRepository extends JpaRepository<Stock, Long> {
    Page<Stock> findAllByIdGreaterThanEqualOrderByStockQuantity (long id, Pageable pageable);

    Stock findByProduct (Product product);
}
