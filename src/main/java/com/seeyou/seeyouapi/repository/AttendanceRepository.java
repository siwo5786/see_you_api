package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.entity.Member;
import io.swagger.models.auth.In;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    Page<Attendance> findAllByIdGreaterThanEqualOrderByIdDesc (long id, Pageable pageable);
    // 공부하기
    // 토큰으로 주는건 맴버 그 자체니까 Id가 아닌 맴버로 받는다.
    Optional<Attendance> findByDateAttendanceAndMemberAndIsComplete (LocalDate dateAttendance, Member member, Boolean isComplete);


}
