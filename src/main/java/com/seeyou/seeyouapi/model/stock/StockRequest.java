package com.seeyou.seeyouapi.model.stock;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StockRequest {
    @ApiModelProperty(notes = "재고 수량")
    @NotNull
    @Min(0)
    private Integer StockQuantity;

    @ApiModelProperty(notes = "최소 재고 수량")
    @NotNull
    @Min(0)
    private Integer minQuantity;
}
