package com.seeyou.seeyouapi.model.sell;

import com.seeyou.seeyouapi.entity.Sell;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellItem {
    @ApiModelProperty(notes = "판매 아이디")
    private Long sellId;

    @ApiModelProperty(notes = "상품 이름")
    private String productName;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "금액")
    private Double price;

    @ApiModelProperty(notes = "판매 날짜")
    private String dateSell;

    @ApiModelProperty(notes = "환불 날짜")
    private String dateRefund;

    @ApiModelProperty(notes = "판매 상태")
    private Boolean isComplete;

    private SellItem(Builder builder) {
        this.sellId = builder.sellId;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.dateSell = builder.dateSell;
        this.dateRefund = builder.dateRefund;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<SellItem> {
        private final Long sellId;
        private final String productName;
        private final Integer quantity;
        private final Double price;
        private final String dateSell;
        private final String dateRefund;
        private final Boolean isComplete;

        public Builder(Sell sell) {
            this.sellId = sell.getId();
            this.productName = sell.getProduct().getProductName();
            this.quantity = sell.getQuantity();
            this.price = sell.getPrice();
            this.dateSell = sell.getDateSell();
            this.dateRefund = sell.getDateRefund();
            this.isComplete = sell.getIsComplete();
        }

        @Override
        public SellItem build() {
            return new SellItem(this);
        }
    }
}
