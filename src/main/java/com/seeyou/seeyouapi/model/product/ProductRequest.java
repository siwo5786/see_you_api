package com.seeyou.seeyouapi.model.product;

import com.seeyou.seeyouapi.enums.ProductType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductRequest {

    @ApiModelProperty(value = "상품 타입")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ProductType productType;

    @ApiModelProperty(value = "상품 이름")
    @Length(min = 2, max = 20)
    @NotNull
    private String productName;

    @ApiModelProperty(value = "단가")
    @NotNull
    private Double unitPrice;

}
