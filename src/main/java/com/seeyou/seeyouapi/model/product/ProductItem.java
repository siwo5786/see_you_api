package com.seeyou.seeyouapi.model.product;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.enums.ProductType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductItem {


    private Long id;

    private String productType;

    private String productName;

    private Double unitPrice;

    private Boolean isEnabled;

    private ProductItem(Builder builder) {
        this.id = builder.id;
        this.productType = builder.productType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;


    }
    public static class Builder implements CommonModelBuilder<ProductItem> {

        private final Long id;

        private final String productType;

        private final String productName;

        private final Double unitPrice;

        private final Boolean isEnabled;

        public Builder(Product product) {
            this.id = product.getId();
            this.productType = product.getProductType().getName();
            this.productName = product.getProductName();
            this.unitPrice = product.getUnitPrice();
            this.isEnabled = product.getIsEnabled();

        }


        @Override
        public ProductItem build() {
            return new ProductItem(this);
        }
    }
}
