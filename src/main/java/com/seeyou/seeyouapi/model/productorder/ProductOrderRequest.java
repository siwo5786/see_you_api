package com.seeyou.seeyouapi.model.productorder;

import com.seeyou.seeyouapi.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class ProductOrderRequest {
    @ApiModelProperty(notes = "수량", required = true)
    @NotNull
    private Integer quantity;
}
