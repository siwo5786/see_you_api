package com.seeyou.seeyouapi.model.attendance;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class AttendanceItem {

    private Long id;

    private String name;

    private LocalDate dateAttendance;

    private LocalTime timeAttendance;

    private AttendanceItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.dateAttendance = builder.dateAttendance;
        this.timeAttendance = builder.timeAttendance;

    }

    public static class Builder implements CommonModelBuilder<AttendanceItem> {

        private final Long id;

        private final String name;

        private final LocalDate dateAttendance;

        private final LocalTime timeAttendance;

        public Builder(Attendance attendance) {
            this.id = attendance.getId();
            this.name = attendance.getMember().getName();
            this.dateAttendance = attendance.getDateAttendance();
            this.timeAttendance = attendance.getTimeAttendance();

        }


        @Override
        public AttendanceItem build() {
            return new AttendanceItem(this);
        }
    }

}
